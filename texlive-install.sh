#!/bin/sh

######################################################################
RED='\033[0;33m'
NC='\033[0m' # No Color

######################################################################

dir=$(pwd)

echo -e ${RED}
echo [gotham-install] LINK FILES TO LOCAL TEXMF
echo [gotham-install] Initialize usertree
echo -e ${NC}
tlmgr init-usertree

#cd ~/texmf
cd $(kpsewhich -var-value TEXMFHOME) 
mkdir -p tex/latex/local/beamer/ && cd tex/latex/local/beamer/

######################################################################
echo -e ${RED}
echo [gotham-install] source directory: $dir
echo [gotham-install] destination dir : $(pwd)
echo -e ${NC}

for file in $dir/*.sty 
do
 echo $file
 ln -s $file .
done


echo -e ${RED}
echo [gotham-install] rebuild index
echo -e ${NC}
texhash ~/texmf

echo -e ${RED}
echo [gotham-install] Over. 
echo -e ${NC}
