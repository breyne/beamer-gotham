# beamer-gotham

*Metropolis' ugly sister*.
A beamer theme aiming for simplicity, clarity and consistency.

- Meant for scientific presentations.
- Gathers multiple tips and tweaks, all sources cited with implementation.
- Several light/dark color-schemes and options.
- A lot is inspired from metropolis.

More comments and example in `gotham-example.{tex,pdf}` (requires `biber`, compatible with `pdflatex` and `lualatex`).
